import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import IconButton from '@mui/material/IconButton';
import SearchMenu from '../../components/SearchMenu';
import Results from '../../components/Results';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import API_URL from "../../utils/constants";
import Moment from 'moment';



const Home = () => {
  const [articles, setArticles] = useState([]);
  const[searchStr, setsearchStr]= useState<string | null>();

  interface Article {
    id: number;
    title: string;
    summary: string;
    imageUrl: string;
    updatedAt: string;
  }

  useEffect(() => {
    axios.get(API_URL)
      .then(res => {
        setArticles(res.data.slice(0, 6))
      }).catch(err => console.log(err))
  }, []);
  
  const getText = (dataStr: { title: string}) => {
    // console.log(dataStr.title.split(searchStr))
    const parsed = dataStr.title
    .replaceAll(searchStr, `**${searchStr}**`)
    .split('**')
    .map((el: string) => el.trim() === searchStr ? (<span style={{background: 'yellow'}}>{searchStr}</span>): el)
    
    return parsed;
  }
  return (
    <Box sx={{ top: '50px', position: 'absolute' }}>
      <SearchMenu searchStr={searchStr} onChange={value => setsearchStr(value)} />
      <Results />
      <Grid container spacing={{ xs: 2 }}>
        {articles.map((article: Article) => (
          <Grid item key={article.id} xs={2} sm={4} md={4}>
            <Card
              sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}
            >
              <CardMedia
                component="img"
                width="400"
                height="220"
                image={article.imageUrl}
                alt="image"
              />
              <CardContent sx={{ flexGrow: 1 }}>
                <Typography>
                  {Moment(article.updatedAt).format('DD MMMM YYYY')}
                </Typography>
                <Typography gutterBottom variant="h5" component="h2">
                  {searchStr ? getText(article) : article.title}
                </Typography>
                <Typography>
                  {article.summary}
                </Typography>
              </CardContent>
              <CardActions>
                <Link to={`articles/${article.id}`}>
                  <Button size="small">Read more</Button>
                  <IconButton color="primary" sx={{ p: '10px' }} aria-label="directions">
                    <ArrowForwardIcon />
                  </IconButton>
                </Link>
              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Box>
  );
}

export default Home;