import { Box, CardMedia } from "@mui/material"
import Link from '@mui/material/Link';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import { useParams } from "react-router-dom";
import axios from "axios";
import { useEffect, useState } from "react";
import API_URL from "../../utils/constants";


const Article = () => {

  interface Article {
    id: number;
    title: string;
    summary: string;
    imageUrl: string;
  }

  let { id } = useParams();
  const [article, setArticle] = useState<Article>();

  useEffect(() => {
    axios.get(`${API_URL}/${id}`)
      .then(res => {
        // console.log(res.data)
        setArticle(res.data)
      }).catch(err => console.log(err))
  }, []);

  return (
    <Box sx={{ position: 'relative' }}>

      <CardMedia
        component="img"
        width="400"
        height="220"
        image={article?.imageUrl}
        alt="image"
      />
      <Paper sx={{ position: 'absolute', p: 5, top: '150px', left: '75px', width: '1000px', height: '1000px' }}>
        <Typography sx={{ height: '50px', textAlign: 'center', fontSize: '1.2rem' }}>
          {article?.title}
        </Typography>
        <Typography>
          {article?.summary}
        </Typography>
      </Paper>
      <Link href="/">
        <Typography sx={{ display: 'flex', alignItems: 'center', position: 'absolute', top: '1186px' }}>
          <ArrowBackIcon />
          Back to homepage
        </Typography>
      </Link>
    </Box>
  )
}

export default Article;