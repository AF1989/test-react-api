import Home from './pages/Home';
import Article from './pages/Article';
import { CssBaseline, Container } from '@mui/material';
import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";



function App() {
  return (
    <BrowserRouter>
      <CssBaseline />
      <Container sx={{ maxWidth: 'lg', position: 'relative' }}>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="articles/:id" element={<Article />} />
        </Routes>
      </Container>
    </BrowserRouter>
  );
}

export default App;
