import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import SearchIcon from '@mui/icons-material/Search';
import Box from '@mui/material/Box';
import { InputBase } from '@mui/material';


type SearchStrT = string | null;
type SearchMenuT = { searchStr: SearchStrT, onChange: (val: string | null) => void };

const SearchMenu = ({ searchStr, onChange }: SearchMenuT) => (
    <Box>
      Filter by keywords
      <Grid item sx={{width: '600px', height: '60px'}}>
        <Paper sx={{ p: '2px 4px', display: 'flex', alignItems: 'center' }}>
          <SearchIcon />
          <InputBase
            sx={{ ml: 1, flex: 1 }}
            placeholder="Put the search name or description"
            onChange={(event) => onChange(event.target.value)}
            value={searchStr || ''}
          />
        </Paper>
      </Grid>
    </Box>
  )

export default SearchMenu;